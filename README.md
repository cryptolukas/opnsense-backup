## OPNsense Backup to Backblaze

Upload your OPNsense Backups to Backblaze

Prerequisite

* Running OPNsense
* ssh-key configured with a opnsense user
* ansible installed

1. Prepare Ansible

Fill the [secets.yml](ansible/backup/vars/secrets.yml) with your Security Credentials from Backblaze

You can get the Opnsense API keys as described [here](https://docs.opnsense.org/development/how-tos/api.html#creating-keys)

You can get the Backblaze App keys as described [here](https://support.promax.com/knowledge/backblaze-b2)

```
---
opnsense_backup_backblaze_key_id:
opnsense_backup_backblaze_key:
opnsense_backup_opnsense_key:
opnsense_backup_opnsense_key_secret:
```

```
ansible-vault encrypt ansible/backup/vars/secrets.yml
```

Fill the hosts file with your opnsense
```
opnsense.example.com ansible_user=OPNSENSE_USER
```

Set the Backblaze Bucket Name in site.yml
```
  vars:
    opnsense_backup_bucket: BUCKET_NAME
    opnsense_backup_bucket_folder: FOLDERNAME IN BUCKET
```

Execute the playbook
```
ansible-playbook -D -i ansible/hosts ansible/site.yml --ask-vault-pass
```
